// Replay plays back data from one or more archive files through
// Redis pub-sub channels at a specified interval.
package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/imdario/mergo"
)

type dataChannel struct {
	Name     string `toml:"name"`
	Prefix   string `toml:"prefix"`
	Interval string `toml:"interval"`
}

type redisConfig struct {
	Address string        `toml:"address"`
	Chans   []dataChannel `toml:"chans"`
}

type sysConfig struct {
	Redis redisConfig `toml:"redis"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: replay [options] file [file ...]

Plays back data from one or more archive files through Redis pub-sub
channels at a specified interval.
`

var DEFAULT = `
[redis]
address = "localhost:6379"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	config = flag.String("config", "", "Configuration file")
)

func dataSource(ctx context.Context, pool *redis.Pool, rdr io.Reader,
	channel string,
	interval time.Duration) {

	conn := pool.Get()
	defer conn.Close()
	scanner := bufio.NewScanner(rdr)
	for scanner.Scan() {
		select {
		case <-ctx.Done():
			return
		case <-time.After(interval):
		}
		conn.Do("PUBLISH", channel, scanner.Bytes())
	}

	if err := scanner.Err(); err != nil {
		log.Printf("Error reading archive file: %v", err)
	}
}

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
		os.Exit(1)
	}

	var (
		err         error
		defcfg, cfg sysConfig
	)

	if err = toml.Unmarshal([]byte(DEFAULT), &defcfg); err != nil {
		log.Fatalf("Cannot parse default configuration: %v", err)
	}

	if *config != "" {
		log.Println("Reading configuration file")
		b, err := ioutil.ReadFile(*config)
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}

		if err = toml.Unmarshal(b, &cfg); err != nil {
			log.Fatalf("Cannot parse configuration file: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)
	if len(cfg.Redis.Chans) == 0 {
		log.Fatal("No Redis channels specified")
	}

	pool := newPool(cfg.Redis.Address)
	table := make(map[string]struct {
		channel  string
		interval time.Duration
	})

	for _, dc := range cfg.Redis.Chans {
		t := table[dc.Prefix]
		t.channel = dc.Name
		t.interval, err = time.ParseDuration(dc.Interval)
		if err != nil {
			log.Fatalf("Bad interval: %q (%v)", dc.Interval, err)
		}
		table[dc.Prefix] = t
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	var wg sync.WaitGroup
	for _, filename := range args {
		parts := strings.Split(filename, "-")
		if entry, ok := table[parts[0]]; ok {
			if f, err := os.Open(filename); err == nil {
				wg.Add(1)
				go func() {
					defer wg.Done()
					dataSource(ctx, pool, f, entry.channel, entry.interval)
				}()
			} else {
				log.Printf("Cannot open %q (%v)", filename, err)
			}
		} else {
			log.Printf("Prefix not in config file: %q", parts[0])
		}
	}
	log.Println("Waiting for tasks to finish ...")
	wg.Wait()
}
