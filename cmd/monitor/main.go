// Package main implements a program which monitors the movement of the
// ICEX Tracking Range grid.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/signal"
	"runtime"
	"sort"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/imdario/mergo"
)

type redisConfig struct {
	Address string            `toml:"address"`
	Chans   map[string]string `toml:"chans,omitempty"`
	Keys    map[string]string `toml:"keys,omitempty"`
}

type sysConfig struct {
	Redis  redisConfig            `toml:"redis"`
	Params map[string]interface{} `toml:"params"`
}

// Alert messages published on Redis
type Alert struct {
	Timestamp int64  `json:"timestamp"`
	Text      string `json:"text"`
}

type rangeGrid struct {
	pool *redis.Pool
	// Redis key for reference Range Matrix
	rmkey string
	// Name of Redis pub-sub channel for alert messages
	alert_chan string
	// Name of Redis pub-sub channel for range/bearing offsets
	offset_chan string
	// Maximum allowed range difference (in mm)
	max_range uint32
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: monitor [options] [configfile]

Monitor the ICEX grid motion and orientation relative to the most
recent survey and publish the information to a Redis pub-sub channel.
`

var DEFAULT = `
[redis]
address = "localhost:6379"
[redis.chans]
gps = "data.refs"
ranges = "data.ranges"
grid = "data.grid"
offsets = "data.offsets"
alerts = "grid.alert"
[redis.keys]
ranges = "ranges"
[params]
range_change = 3
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
)

// Return the difference of two Range Matrix structs. The ranges in the
// difference matrix are the absolute values of the difference in
// ranges. The azimuth differences are signed.
func matrixDiff(rm0 *api.RangeMatrix, rm1 *api.RangeMatrix) *api.RangeMatrix {
	var (
		x, y float64
		daz  int32
	)

	np := int(rm0.N * rm0.N)
	result := api.RangeMatrix{N: rm0.N}
	result.Ids = make([]string, rm0.N)
	result.Ranges = make([]uint32, np)
	result.Azimuths = make([]int32, np)
	result.Tsec = rm1.Tsec
	copy(result.Ids, rm0.Ids)

	for i := 0; i < np; i++ {
		x = float64(rm0.Ranges[i])
		y = float64(rm1.Ranges[i])
		result.Ranges[i] = uint32(math.Abs(y - x))
		daz = rm1.Azimuths[i] - rm0.Azimuths[i]
		// Map azimuth to [-180, 180]
		if daz > int32(180*api.ANGLESCALE) {
			daz = daz - int32(360*api.ANGLESCALE)
		} else if daz < int32(-180*api.ANGLESCALE) {
			daz = daz + int32(360*api.ANGLESCALE)
		}
		result.Azimuths[i] = daz
	}

	return &result
}

// Check the status of the grid and return a channel which will provide the
// new (scaled) value of the grid azimuth.
func (rg *rangeGrid) checkGrid(rm *api.RangeMatrix) <-chan int32 {
	c := make(chan int32, 1)

	go func() {
		defer close(c)

		conn := rg.pool.Get()
		defer conn.Close()

		// Retrieve the reference value of the Range Matrix
		val, err := redis.String(conn.Do("LINDEX", rg.rmkey, -1))
		if err != nil {
			log.Printf("checkGrid error: %v", err)
			return
		}
		rm0 := api.RangeMatrix{}
		err = jsonpb.UnmarshalString(val, &rm0)
		if err != nil {
			log.Printf("checkGrid error: %v", err)
			return
		}

		ma := jsonpb.Marshaler{EmitDefaults: true}
		diff := matrixDiff(&rm0, rm)
		if rg.offset_chan != "" {
			if msg, err := ma.MarshalToString(diff); err == nil {
				conn.Do("PUBLISH", rg.offset_chan, msg)
			}
		}

		daz := make([]int, diff.N-1)
		// Check the first row of the matrix for the range and bearing
		// changes relative to the origin.
		for i := 1; i < int(diff.N); i++ {
			if rg.alert_chan != "" && diff.Ranges[i] > rg.max_range {
				a := &Alert{
					Timestamp: time.Now().UTC().Unix(),
					Text: fmt.Sprintf("%q: %.1f m range change",
						diff.Ids[i], float64(diff.Ranges[i])/api.GRIDSCALE),
				}
				log.Println(a.Text)
				msg, err := json.Marshal(a)
				if err != nil {
					log.Printf("Cannot encode message: %v", err)
				} else {
					conn.Do("PUBLISH", rg.alert_chan, msg)
				}
			}
			daz[i-1] = int(diff.Azimuths[i])
		}
		sort.Ints(daz)
		c <- int32(daz[diff.N/2])
	}()

	return c
}

// Read messages from the GPS and Ranges Redis channels and use the information to
// update the grid settings.
func gridUpdate(psc redis.PubSubConn, rg *rangeGrid,
	rangechan, gpschan string) <-chan api.Grid {
	c := make(chan api.Grid, 1)

	go func() {
		defer close(c)
		var (
			azChan <-chan int32
			grid   api.Grid
			rm     api.RangeMatrix
			refs   api.PointSet
		)

		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				switch msg.Channel {
				case rangechan:
					err := jsonpb.UnmarshalString(string(msg.Data), &rm)
					if err != nil {
						log.Printf("JSON decode error (data.ranges): %v", err)
					}
					azChan = rg.checkGrid(&rm)
				case gpschan:
					err := jsonpb.UnmarshalString(string(msg.Data), &refs)
					if err != nil {
						log.Printf("JSON decode error (data.refs): %v", err)
					}
					grid.Tsec = time.Now().UTC().Unix()
					grid.Origin = refs.Points["origin"]
					// If range information has been updated, read the new azimuth value
					// otherwise the previous value is kept.
					if azChan != nil {
						grid.Azimuth = <-azChan
						azChan = nil
					}
					c <- grid
				}
			}
		}
	}()

	return c
}

func new_pool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err         error
		defcfg, cfg sysConfig
		ok          bool
	)

	if err = toml.Unmarshal([]byte(DEFAULT), &defcfg); err != nil {
		log.Fatalf("Cannot parse default configuration: %v", err)
	}

	args := flag.Args()

	if len(args) > 0 {
		log.Println("Reading configuration file")
		b, err := ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}

		if err = toml.Unmarshal(b, &cfg); err != nil {
			log.Fatalf("Cannot parse configuration file: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)

	rg := rangeGrid{}
	rg.pool = new_pool(cfg.Redis.Address)
	if rg.rmkey, ok = cfg.Redis.Keys["ranges"]; !ok {
		log.Fatalf("\"ranges\" key must be specified")
	}
	if rg.alert_chan, ok = cfg.Redis.Chans["alerts"]; !ok {
		log.Fatalf("\"alerts\" channel must be specified")
	}
	rg.offset_chan = cfg.Redis.Chans["offsets"]
	if _, ok = cfg.Params["range_change"].(float64); ok {
		rg.max_range = uint32(cfg.Params["range_change"].(float64) * api.GRIDSCALE)
	} else {
		rg.max_range = uint32(3.0 * api.GRIDSCALE)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	psconn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	defer psconn.Close()
	psc := redis.PubSubConn{Conn: psconn}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	c := gridUpdate(psc, &rg, cfg.Redis.Chans["ranges"], cfg.Redis.Chans["gps"])
	psc.Subscribe(cfg.Redis.Chans["ranges"], cfg.Redis.Chans["gps"])

	ma := jsonpb.Marshaler{EmitDefaults: true}
	conn := rg.pool.Get()
	for grid := range c {
		msg, err := ma.MarshalToString(&grid)
		if err != nil {
			log.Printf("Encoding error: %v", err)
		} else {
			conn.Do("PUBLISH", cfg.Redis.Chans["grid"], msg)
			if *debug {
				log.Printf("%q\n", msg)
			}
		}
	}
}
