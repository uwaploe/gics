package main

import (
	"sort"
	"testing"

	"bitbucket.org/uwaploe/gics/api"
)

func TestRangeDiff(t *testing.T) {
	rm0 := api.RangeMatrix{
		N:   5,
		Ids: []string{"origin", "h1", "h2", "h3", "h4"},
		Ranges: []uint32{
			0, 250000, 250000, 250000, 250000,
			250000, 0, 400000, 500000, 300000,
			250000, 400000, 0, 300000, 500000,
			250000, 500000, 300000, 0, 400000,
			250000, 300000, 500000, 400000, 0,
		},
		Azimuths: []int32{
			0, -36870, -143130, 143130, -36870,
			143130, 0, 180000, 143130, 90000,
			36870, 0, 0, 90000, 36870,
			-36870, -36870, -90000, 0, 0,
			-143130, -90000, -143130, 180000, 0,
		},
	}

	rm1 := api.RangeMatrix{
		N:   5,
		Ids: []string{"origin", "h1", "h2", "h3", "h4"},
		Ranges: []uint32{
			0, 250000, 250000, 250000, 250000,
			250000, 0, 400000, 500000, 300000,
			250000, 400000, 0, 300000, 500000,
			250000, 500000, 300000, 0, 400000,
			250000, 300000, 500000, 400000, 0,
		},
		Azimuths: []int32{
			0, -41870, -148130, 138130, -41870,
			143130, 0, 180000, 143130, 90000,
			36870, 0, 0, 90000, 36870,
			-36870, -36870, -90000, 0, 0,
			-143130, -90000, -143130, 180000, 0,
		},
	}

	drm := matrixDiff(&rm0, &rm1)
	daz := make([]int, drm.N-1)
	for i := 1; i < int(drm.N); i++ {
		daz[i-1] = int(drm.Azimuths[i])
	}

	sort.Ints(daz)
	idx := drm.N / 2
	if daz[idx] != -5000 {
		t.Errorf("Bad azimuth value: %d", daz[idx])
	}

}

func TestChanLogic(t *testing.T) {
	c := make(chan float64, 1)

	go func() {
		defer close(c)
	}()

	if _, ok := <-c; ok {
		t.Error("Empty channel not detected")
	}

	c = make(chan float64, 1)

	go func() {
		defer close(c)
		c <- 42.0
	}()

	if x, ok := <-c; !ok || x != 42.0 {
		t.Errorf("Channel read failed")
	}

}
