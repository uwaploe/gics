// Genkml creates a KML file from one or more ICEX Grid CSV files.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"time"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: genkml [options] gridfile [gridfile ...]

Create a KML file from one or more ICEX Grid CSV files.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	interval = flag.Duration("interval", 60*time.Second,
		"Minimum interval between Camp track positions")
	output  = flag.String("outfile", "", "Output file name")
	docname = flag.String("docname", "Ice Camp", "KML Document object name")
)

type geoPoint struct {
	Lat       float64
	Lon       float64
	Heading   float64
	Timestamp time.Time
}

type kmlDesc struct {
	Name    string
	Tstart  time.Time
	Tend    time.Time
	View    geoPoint
	Points  []geoPoint
	Markers []*geoPoint
}

// Read all data records from a Grid CSV file
func (k *kmlDesc) readPoints(r io.Reader, hasHeader bool) error {
	var (
		t      string
		last_t time.Time
	)
	scanner := bufio.NewScanner(r)
	line := int(0)
	for scanner.Scan() {
		line++
		if hasHeader && line == 1 {
			continue
		}
		p := geoPoint{}
		_, err := fmt.Sscanf(scanner.Text(), "%20s,%f,%f,%f", &t, &p.Lat, &p.Lon, &p.Heading)
		if err != nil {
			log.Printf("Error on line %d: %v", line, err)
			continue
		}
		p.Timestamp, err = time.Parse(time.RFC3339, t)
		if err != nil {
			log.Printf("Malformed timestamp on line %d: %v", line, err)
			continue
		}
		if p.Timestamp.Sub(last_t) >= *interval {
			k.Points = append(k.Points, p)
			last_t = p.Timestamp
		}
	}

	return scanner.Err()
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing input files ...")
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
		os.Exit(1)
	}

	tmpl := loadTemplate()

	kml := kmlDesc{}
	kml.Points = make([]geoPoint, 0)
	kml.Markers = make([]*geoPoint, 0)
	kml.Name = *docname

	// Load all of the Camp locations
	for _, arg := range args {
		infile, err := os.Open(arg)
		if err != nil {
			log.Fatal(err)
		}
		kml.readPoints(infile, true)
		infile.Close()
	}

	if len(kml.Points) == 0 {
		log.Fatal("No data points!")
	}

	kml.Tstart = kml.Points[0].Timestamp
	kml.Tend = kml.Points[len(kml.Points)-1].Timestamp
	kml.View = kml.Points[len(kml.Points)-1]

	// Add daily markers
	marker_interval := time.Hour * 24
	j := int(0)
	kml.Markers = append(kml.Markers, &kml.Points[0])
	for i, p := range kml.Points {
		if p.Timestamp.Sub(kml.Markers[j].Timestamp) >= marker_interval {
			kml.Markers = append(kml.Markers, &kml.Points[i])
			j++
		}
	}

	var (
		w   io.WriteCloser
		err error
	)

	if *output != "" {
		w, err = os.Create(*output)
		if err != nil {
			log.Fatal(err)
		}
		defer w.Close()
	} else {
		w = os.Stdout
	}

	tmpl.Execute(w, kml)
}
