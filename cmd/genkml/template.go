package main

import (
	"fmt"
	"text/template"
	"time"
)

var KML_TEMPLATE = `
<kml xmlns:atom="http://www.w3.org/2005/Atom" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <name>{{.Name}}</name>
    <Style id="track">
      <IconStyle>
        <Icon>
          <href>http://earth.google.com/images/kml-icons/track-directional/track-0.png</href>
        </Icon>
      </IconStyle>
      <LineStyle>
        <color>9959acff</color>
        <width>2</width>
      </LineStyle>
    </Style>
    <Style id="markers">
      <IconStyle>
        <scale>0.6</scale>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/paddle/ylw-circle.png</href>
        </Icon>
      </IconStyle>
      <LabelStyle>
        <scale>0.0</scale>
      </LabelStyle>
      <BalloonStyle>
        <text>
          <![CDATA[
                   <b>Waypoint</b><br/>
                   Daily marker: $[timestamp]<br/>
          ]]>
        </text>
      </BalloonStyle>
    </Style>
    <LookAt>
      <latitude>{{.View.Lat|toDegrees}}</latitude>
      <longitude>{{.View.Lon|toDegrees}}</longitude>
      <range>100000</range>
    </LookAt>

    <Placemark>
      <name>Ice Camp Track</name>
      <description>
            Track between {{.Tstart|toKmlTime}} and {{.Tend|toKmlTime}}.
      </description>
      <styleUrl>#track</styleUrl>
      <LineString>
        <tessellate>1</tessellate>
        <altitudeMode>absolute</altitudeMode>
        <coordinates>
        {{range .Points}}
        {{.Lon|toDegrees}},{{.Lat|toDegrees -}},0
        {{end}}
        </coordinates>
      </LineString>
    </Placemark>

    <Folder>
      <name>Daily Markers</name>
      <styleUrl>#markers</styleUrl>
      <TimeSpan>
        <begin>{{.Tstart|toKmlTime}}</begin>
      </TimeSpan>
      {{range .Markers}}
      <Placemark>
        <name>Waypoint</name>
        <ExtendedData>
          <Data name="timestamp">
            <value>{{.Timestamp|toKmlTime}}</value>
          </Data>
        </ExtendedData>
        <styleUrl>#markers</styleUrl>
        <Point>
          <coordinates>{{.Lon|toDegrees}},{{.Lat|toDegrees}}</coordinates>
        </Point>
        <TimeStamp>
          <when>{{.Timestamp|toKmlTime}}</when>
        </TimeStamp>
      </Placemark>
      {{end}}
    </Folder>
  </Document>
</kml>
`

func toKmlTime(t time.Time) string {
	return t.Format("2006-01-02T15:04:05-07:00")
}

func toDegrees(val float64) string {
	return fmt.Sprintf("%.6f", val)
}

func toAngle(val float64) string {
	return fmt.Sprintf("%.1f", val)
}

func loadTemplate() *template.Template {
	funcMap := template.FuncMap{
		"toDegrees": toDegrees,
		"toAngle":   toAngle,
		"toKmlTime": toKmlTime,
	}
	tmpl := template.Must(template.New("app").Funcs(funcMap).Parse(KML_TEMPLATE))
	return tmpl
}
