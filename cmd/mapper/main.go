// Package main implements a gRPC server to convert between ICEX grid
// coordinates (x, y) and geodetic coordinates (lat, lon).
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/gics/api/service"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

var Version = "dev"
var BuildDate = "unknown"

var (
	port     = flag.Int("port", 10000, "The server port")
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	address = flag.String("rd-address", "localhost:6379", "Redis server address")
	channel = flag.String("rd-channel", "data.grid",
		"Redis pub-sub channel for grid updates")
)

// Read messages from a Redis pub-sub channel and use them to update
// the state of the GridServer
func gridUpdater(psc redis.PubSubConn, svc *service.GridServer) {
	var (
		grid api.Grid
		err  error
	)

	for {
		switch msg := psc.Receive().(type) {
		case error:
			log.Println(msg)
			return
		case redis.Subscription:
			if msg.Count == 0 {
				log.Println("Pubsub channel closed")
				return
			}
		case redis.Message:
			err = jsonpb.UnmarshalString(string(msg.Data), &grid)
			if err != nil {
				log.Printf("JSON decode error (data.grid): %v", err)
			} else {
				svc.UpdateState(&grid)
			}
		}
	}
}

func inteceptor(ctx context.Context, req interface{},
	info *grpc.UnaryServerInfo, handler grpc.UnaryHandler,
) (interface{}, error) {
	start := time.Now()
	resp, err := handler(ctx, req)
	grpclog.Printf("method=%s duration=%s error=%v",
		info.FullMethod,
		time.Since(start),
		err)
	return resp, err
}

func ServerTrace() grpc.ServerOption {
	return grpc.UnaryInterceptor(inteceptor)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] [gridfile]\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "RPC server to convert grid coordinates\n\n")
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	opts := make([]grpc.ServerOption, 0)
	if *debug {
		opts = append(opts, ServerTrace())
	}

	grpcServer := grpc.NewServer(opts...)

	grid_opts := make([]service.GridServerOption, 0)
	if len(args) > 0 {
		grid_opts = append(grid_opts, service.InitialGridFile(args[0]))
	}

	svc, err := service.NewGridServer(grid_opts...)
	if err != nil {
		grpclog.Fatal(err)
	}
	api.RegisterMapperServer(grpcServer, svc)

	psconn, err := redis.Dial("tcp", *address)
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	go gridUpdater(psc, svc)
	psc.Subscribe(*channel)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			grpclog.Printf("Got signal %v", s)
			grpcServer.GracefulStop()
			psc.Unsubscribe()
		}
	}()

	grpcServer.Serve(lis)
}
