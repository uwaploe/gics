FROM golang:alpine AS build-env
# We need to build the executable in the same environment that we
# will run in due to the Cgo dynamic linking requirements.
RUN apk add --no-cache git make gcc musl-dev
RUN apk add --no-cache proj4 proj4-dev \
    --repository http://nl.alpinelinux.org/alpine/edge/testing
ADD . /src
RUN cd /src && make all

FROM alpine:latest
# Need the Proj.4 shared library
RUN apk add --no-cache proj4 \
    --repository http://nl.alpinelinux.org/alpine/edge/testing
RUN mkdir /app
WORKDIR /app
COPY --from=build-env /src/mapper /app/

EXPOSE 10000

CMD ["-port=10000", "-rd-address=redis:6379" ]
ENTRYPOINT ["./mapper"]
