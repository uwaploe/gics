#!/bin/sh
#

set -e

if [ "$1" = "EOR" ]; then
    exec watchxy -redis=$REDIS_HOST -mapper=$MAPPER_HOST EOR,$2,$3 2> /dev/null |\
        while IFS=, read t name lat lon rest; do
            echo "Name,Latitude,Longitude,Alt"
            echo "$name,$lat,$lon,0"
        done
else
    exec "$@"
fi
