// Watchxy accepts the X-Y coordinates and names of one or more grid
// points and writes the corresponding geodetic coordinates to standard
// output whenever the grid location is updated.
package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"

	"bitbucket.org/uwaploe/gics/api"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: watchxy [options] name,x,y [name,x,y ...]

Watchxy accepts the X-Y coordinates and names of one or more grid
points and writes the corresponding geodetic coordinates to standard
output whenever the grid location is updated.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	redis_sv = flag.String("redis", "localhost:6379",
		"Redis server address")
	mapper_sv = flag.String("mapper", "localhost:10000",
		"Mapper gRPC server address")
	channel = flag.String("channel", "data.grid",
		"Redis pub-sub channel for grid updates")
)

func gridReader(psc redis.PubSubConn) <-chan *api.Grid {
	ch := make(chan *api.Grid, 1)
	go func() {
		defer close(ch)
		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				rec := api.Grid{}
				err := jsonpb.UnmarshalString(string(msg.Data), &rec)
				if err != nil {
					log.Printf("JSON decode error: %v", err)
				}
				ch <- &rec
			}

		}
	}()

	return ch
}

func formatPoint(w io.Writer, p *api.Point, d *api.Drift) error {
	lat := float64(p.Latitude) / api.GEOSCALE
	lon := float64(p.Longitude) / api.GEOSCALE
	var hlat, hlon string

	if lat < 0 {
		hlat = "S"
		lat = -lat
	} else {
		hlat = "N"
	}

	if lon < 0 {
		hlon = "W"
		lon = -lon
	} else {
		hlon = "E"
	}

	fmt.Fprintf(w, "%d,%s,%s%.6f,%s%.6f", p.Tsec, p.Id, hlat, lat, hlon, lon)
	if d != nil {
		fmt.Fprintf(w, ",%.1f,%.1f", float64(d.Course)/api.ANGLESCALE,
			float64(d.Speed)/api.GRIDSCALE)
	}
	_, err := w.Write([]byte("\n"))
	return err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	points := make([]api.GridPoint, 0)
	for _, arg := range args {
		fields := strings.Split(arg, ",")
		if len(fields) < 3 {
			log.Fatal("Invalid point format: %q", arg)
		}
		x, err := strconv.ParseFloat(fields[1], 64)
		if err != nil {
			log.Fatalf("Bad coordinate: %q (%v)", fields[1], err)
		}
		y, err := strconv.ParseFloat(fields[2], 64)
		if err != nil {
			log.Fatalf("Bad coordinate: %q (%v)", fields[2], err)
		}
		p := api.GridPoint{
			X:  int32(x * api.GRIDSCALE),
			Y:  int32(y * api.GRIDSCALE),
			Id: fields[0],
		}
		points = append(points, p)
	}

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(*mapper_sv, opts...)
	if err != nil {
		log.Fatalf("Cannot connect to Mapper service: %v", err)
	}
	defer conn.Close()
	client := api.NewMapperClient(conn)

	psconn, err := redis.Dial("tcp", *redis_sv)
	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	req := api.Empty{}
	ch := gridReader(psc)
	psc.Subscribe(*channel)
	for rec := range ch {
		if *debug {
			log.Printf("%+v", rec)
		}
		for _, gp := range points {
			gp.Tsec = rec.Tsec
			p, err := client.ToGeo(context.Background(), &gp)
			if err != nil {
				log.Println(err)
			} else {
				d, _ := client.GetDrift(context.Background(), &req)
				formatPoint(os.Stdout, p, d)
			}
		}
	}

}
