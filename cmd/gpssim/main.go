// Gpssim simulates the data stream from the ICEX GPS units by playing back
// previously collected data.
package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	gps "bitbucket.org/mfkenney/go-gps"
	"bitbucket.org/mfkenney/go-rfgps"
	"bitbucket.org/uwaploe/gics/api"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/pkg/errors"
)

var usage = `
Usage: gpssim [options] gps_file rfgps_file

Publish simulated ICEX GPS data messages on a Redis pub-sub channel
using CSV data files from a previous deployment.
`

var (
	channel = flag.String("channel", "data.refs",
		"Redis pub-sub channel name")
	server = flag.String("server", "localhost:6379",
		"network address of the Redis server")
)

type ComboRecord struct {
	origin *gps.Record
	nodes  []*rfgps.Record
}

func (cr *ComboRecord) UpdateTimes(dt time.Duration) {
	cr.origin.T = cr.origin.T.Add(dt)
	for _, node := range cr.nodes {
		node.T = node.T.Add(dt)
	}
}

func processRfgpsRecord(rec *rfgps.Record) *api.Point {
	return &api.Point{
		Latitude:  int32(rec.Lat * api.GEOSCALE),
		Longitude: int32(rec.Lon * api.GEOSCALE),
		Tsec:      rec.T.Unix(),
	}
}

func processGpsRecord(rec *gps.Record) *api.Point {
	return &api.Point{
		Latitude:  int32(rec.Lat * api.GEOSCALE),
		Longitude: int32(rec.Lon * api.GEOSCALE),
		Tsec:      rec.T.Unix(),
	}
}

func nextRecord(fgps, frfgps *csv.Reader, conn redis.Conn) (*ComboRecord, error) {
	cr := ComboRecord{}
	cr.nodes = make([]*rfgps.Record, 4)

	input, err := fgps.Read()
	if err != nil {
		return nil, errors.Wrap(err, "gps record read")
	}
	cr.origin = &gps.Record{}
	err = cr.origin.UnmarshalCsv(input)
	if err != nil {
		return nil, errors.Wrap(err, "gps record parse")
	}

	if msg, err := json.Marshal(cr.origin); err == nil {
		conn.Do("PUBLISH", "data.gps", msg)
	}

	tref := cr.origin.T.Unix() / 10

	i := 0
	for i < 4 {
		input, err = frfgps.Read()
		if err != nil {
			return nil, errors.Wrap(err, "rfgps record read")
		}
		rec := &rfgps.Record{}
		err = rec.UnmarshalCsv(input)
		if err != nil {
			return nil, errors.Wrap(err, "rfgps record parse")
		}

		if i == 0 && rec.T.After(cr.origin.T) {
			return nil, errors.New("Time mismatch")
		}

		if msg, err := json.Marshal(rec); err == nil {
			conn.Do("PUBLISH", "data.rfgps", msg)
		}

		if (rec.T.Unix() / 10) == tref {
			cr.nodes[i] = rec
			i++
		}
	}

	return &cr, nil
}

func streamData(ctx context.Context, fgps, frfgps *csv.Reader,
	conn redis.Conn) <-chan *ComboRecord {
	c := make(chan *ComboRecord, 1)
	go func() {
		defer close(c)
		cr, err := nextRecord(fgps, frfgps, conn)
		if err != nil {
			log.Print(err)
			return
		}

		var t0, t1 time.Time

		t0 = cr.origin.T
		cr.UpdateTimes(time.Now().UTC().Sub(t0))
		c <- cr
		for {
			cr, err = nextRecord(fgps, frfgps, conn)
			if err != nil {
				log.Print(err)
				return
			}
			t1 = cr.origin.T
			select {
			case <-ctx.Done():
				return
			case <-time.After(t1.Sub(t0)):
			}
			cr.UpdateTimes(time.Now().UTC().Sub(t1))
			c <- cr
			t0 = t1
		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}
	// Open a second connection for the goroutine
	conn2, _ := redis.Dial("tcp", *server)

	// Open CSV files
	file, err := os.Open(args[0])
	if err != nil {
		log.Fatal(errors.Wrapf(err, "opening %s", args[0]))
	}
	gps := csv.NewReader(file)

	file, err = os.Open(args[1])
	if err != nil {
		log.Fatal(errors.Wrapf(err, "opening %s", args[1]))
	}
	rfgps := csv.NewReader(file)

	// Skip the header line in each file
	gps.Read()
	rfgps.Read()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	pset := api.PointSet{}
	pset.Points = make(map[string]*api.Point)
	names := []string{"h1", "h2", "h3", "h4"}
	var p *api.Point

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			cancel()
		}
	}()

	ma := jsonpb.Marshaler{EmitDefaults: true}
	c := streamData(ctx, gps, rfgps, conn2)
	for cr := range c {
		p = processGpsRecord(cr.origin)
		p.Id = "origin"
		pset.Points[p.Id] = p
		for i, node := range cr.nodes {
			p = processRfgpsRecord(node)
			p.Id = names[i]
			pset.Points[p.Id] = p
		}
		msg, err := ma.MarshalToString(&pset)
		if err != nil {
			log.Printf("Encoding error: %v", err)
		} else {
			conn.Do("PUBLISH", *channel, msg)
		}
	}
}
