// Archiver subscribes to one or more Redis pub-sub channels and stores
// the messages to a series of newline-delimited JSON files.
package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"sync"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/imdario/mergo"
)

type dataChannel struct {
	Name   string `toml:"name"`
	Prefix string `toml:"prefix"`
}

type redisConfig struct {
	Address string        `toml:"address"`
	Chans   []dataChannel `toml:"chans"`
}

type sysConfig struct {
	Params map[string]string `toml:"params"`
	Redis  redisConfig       `toml:"redis"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: archiver [options] [configfile]

Subscribes to one or more Redis pub-sub channels and stores the messages
to a series of newline-delimited JSON files.
`

var DEFAULT = `
[params]
topdir = "/data/ICEX"
interval = "6h"
[redis]
address = "localhost:6379"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
)

// Pass all received Redis pub-sub messages to a Go channel.
func dataReader(psc redis.PubSubConn) <-chan redis.Message {
	c := make(chan redis.Message, 1)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				c <- msg
			}
		}
	}()

	return c
}

func newArchiveFile(pathname string) (*os.File, error) {
	log.Printf("Opening %q", pathname)
	// Check if file already exists
	_, err := os.Stat(pathname)
	newfile := os.IsNotExist(err)

	if newfile {
		// Create all directories if needed. We can safely ignore the
		// returned error because it will be caught when we try to
		// open the file.
		os.MkdirAll(filepath.Dir(pathname), 0755)
	}

	return os.OpenFile(pathname, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
}

// Archiver accepts byte slices from a channel and appends them to a
// file followed by a linefeed character.
func archiver(ctx context.Context, src <-chan []byte, pathname string) {
	log.Printf("archiver routine starting (%s)", pathname)
	f, err := newArchiveFile(pathname)
	if err != nil {
		log.Printf("Cannot open file %s: %v", pathname, err)
		return
	}
	defer f.Close()

	wbuf := bufio.NewWriter(f)
	defer wbuf.Flush()

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case rec, ok := <-src:
			if ok {
				wbuf.Write(rec)
				wbuf.WriteByte('\n')
				err = wbuf.Flush()
				if err != nil {
					log.Printf("Write error on %s: %v", pathname, err)
				}
			} else {
				break loop
			}

		}
	}
	log.Printf("archiver routine done (%s)", pathname)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err         error
		defcfg, cfg sysConfig
		interval    time.Duration
		topdir      string
		ok          bool
	)

	if err = toml.Unmarshal([]byte(DEFAULT), &defcfg); err != nil {
		log.Fatalf("Cannot parse default configuration: %v", err)
	}

	args := flag.Args()

	if len(args) > 0 {
		log.Println("Reading configuration file")
		b, err := ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}

		if err = toml.Unmarshal(b, &cfg); err != nil {
			log.Fatalf("Cannot parse configuration file: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)

	if len(cfg.Redis.Chans) == 0 {
		log.Print("No channels to monitor, exiting ...")
		os.Exit(0)
	}

	if val, ok := cfg.Params["interval"]; ok {
		interval, err = time.ParseDuration(val)
		if err != nil {
			log.Fatalf("Cannot parse interval: %v", err)
		}
	} else {
		log.Fatal("Roll-over interval not specified!")
	}

	if topdir, ok = cfg.Params["topdir"]; !ok {
		log.Fatal("Archive directory not specified!")
	}

	// Archive file naming scheme
	var archive_name func(string) string
	if interval >= (time.Hour * 24) {
		archive_name = func(prefix string) string {
			t := time.Now().UTC()
			return fmt.Sprintf("%s/%d/%d/%s-%s.json",
				topdir,
				t.Year(),
				t.YearDay(),
				prefix,
				t.Format("20060102"))
		}
	} else if interval >= time.Hour {
		archive_name = func(prefix string) string {
			t := time.Now().UTC()
			return fmt.Sprintf("%s/%d/%d/%s-%s.json",
				topdir,
				t.Year(),
				t.YearDay(),
				prefix,
				t.Format("20060102-15"))
		}
	} else {
		archive_name = func(prefix string) string {
			t := time.Now().UTC()
			return fmt.Sprintf("%s/%d/%d/%s-%s.json",
				topdir,
				t.Year(),
				t.YearDay(),
				prefix,
				t.Format("2006010215-04"))
		}
	}

	var (
		ctx, ar_ctx context.Context
		cancel      context.CancelFunc
		wg          sync.WaitGroup
	)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	psconn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	defer psconn.Close()
	psc := redis.PubSubConn{Conn: psconn}

	// Top level context
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
			cancel()
		}
	}()

	datasrc := dataReader(psc)

	// Map a Go channel and a file-name prefix to each
	// Redis pub-sub channel.
	chmap := make(map[string]chan []byte)
	prmap := make(map[string]string)
	for _, dc := range cfg.Redis.Chans {
		chmap[dc.Name] = make(chan []byte, 1)
		defer close(chmap[dc.Name])
		prmap[dc.Name] = dc.Prefix
	}

	deadline := time.Now().Truncate(interval).Add(interval)
	log.Printf("Next deadline: %v", deadline)
	ar_ctx, _ = context.WithDeadline(ctx, deadline)
	for k, ch := range chmap {
		wg.Add(1)
		go func(ch chan []byte, k string) {
			defer wg.Done()
			archiver(ar_ctx, ch, archive_name(prmap[k]))
		}(ch, k)
		psc.Subscribe(k)
	}

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case <-ar_ctx.Done():
			if ar_ctx.Err() == context.DeadlineExceeded {
				// Deadline expired, set the next deadline, wait for archivers
				// to exit and start the next group
				deadline = deadline.Add(interval)
				log.Println("Waiting for tasks to finish ...")
				wg.Wait()
				log.Printf("Next deadline: %v", deadline)
				ar_ctx, _ = context.WithDeadline(ctx, deadline)
				for k, ch := range chmap {
					wg.Add(1)
					go func(ch chan []byte, k string) {
						defer wg.Done()
						archiver(ar_ctx, ch, archive_name(prmap[k]))
					}(ch, k)
				}
			} else {
				// Cancelled
				break loop
			}
		case msg := <-datasrc:
			chmap[msg.Channel] <- msg.Data
		}

	}

	cancel()
	log.Println("Waiting for tasks to finish ...")
	wg.Wait()
}
