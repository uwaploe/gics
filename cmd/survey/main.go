// Package main implements a gRPC server which performs the initial survey
// of the ICEX local grid
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/gics/api/service"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

var Version = "dev"
var BuildDate = "unknown"

var (
	port     = flag.Int("port", 10001, "The server port")
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug      = flag.Bool("debug", false, "Enable debugging output")
	address    = flag.String("rd-address", "localhost:6379", "Redis server address")
	in_channel = flag.String("in-channel", "data.refs",
		"Input pub-sub channel for reference-point updates")
	out_channel = flag.String("out-channel", "data.ranges",
		"Output pub-sub channel for range-matrix updates")
	refpoints = flag.String("refs", "h1,h2,h3,h4",
		"Survey reference point names")
)

// Read messages from a Redis pub-sub channel and use them to update
// the state of the SurveyServer
func refsUpdater(psc redis.PubSubConn, svc *service.SurveyServer) {
	var (
		pset api.PointSet
		err  error
	)

	ctx := context.Background()

	for {
		switch msg := psc.Receive().(type) {
		case error:
			log.Println(msg)
			return
		case redis.Subscription:
			if msg.Count == 0 {
				log.Println("Pubsub channel closed")
				return
			}
		case redis.Message:
			err = jsonpb.UnmarshalString(string(msg.Data), &pset)
			if err != nil {
				log.Printf("JSON decode error (data.refs): %v", err)
			} else {
				svc.UpdatePoints(ctx, &pset)
			}
		}
	}
}

func inteceptor(ctx context.Context, req interface{},
	info *grpc.UnaryServerInfo, handler grpc.UnaryHandler,
) (interface{}, error) {
	start := time.Now()
	resp, err := handler(ctx, req)
	grpclog.Printf("method=%s duration=%s error=%v",
		info.FullMethod,
		time.Since(start),
		err)
	return resp, err
}

func ServerTrace() grpc.ServerOption {
	return grpc.UnaryInterceptor(inteceptor)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] reffile\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "RPC server for the ICEX grid survey\n\n")
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	psconn, err := redis.Dial("tcp", *address)
	if err != nil {
		grpclog.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Open a second connection to Redis for publishing.
	conn, err := redis.Dial("tcp", *address)
	if err != nil {
		grpclog.Fatal(err)
	}

	publisher := func(rm *api.RangeMatrix) {
		ma := jsonpb.Marshaler{}
		ma.EmitDefaults = true
		if msg, err := ma.MarshalToString(rm); err == nil {
			conn.Do("PUBLISH", *out_channel, msg)
		} else {
			log.Print(err)
		}
	}

	svcopts := make([]service.SurveyServerOption, 0)
	svcopts = append(svcopts, service.Publisher(publisher))
	if *refpoints != "" {
		rp := make([]string, 1)
		rp[0] = "origin"
		for _, s := range strings.Split(*refpoints, ",") {
			rp = append(rp, s)
		}
		svcopts = append(svcopts, service.ReferencePoints(rp))
	}

	svc := service.NewSurveyServer(svcopts...)

	args := flag.Args()
	if len(args) > 0 {
		file, err := ioutil.ReadFile(args[0])
		if err != nil {
			grpclog.Fatalf("Failed to load reference points: %v", err)
		}

		var pset api.PointSet
		if err := jsonpb.UnmarshalString(string(file), &pset); err != nil {
			grpclog.Fatalf("File decode error: %v", err)
		}

		_, err = svc.UpdatePoints(context.Background(), &pset)
		if err != nil {
			grpclog.Fatal(err)
		}
	}

	opts := make([]grpc.ServerOption, 0)
	if *debug {
		opts = append(opts, ServerTrace())
	}

	grpcServer := grpc.NewServer(opts...)
	api.RegisterSurveyorServer(grpcServer, svc)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Monitor pub-sub channel for reference-point updates
	go refsUpdater(psc, svc)
	psc.Subscribe(*in_channel)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			grpclog.Printf("Got signal %v", s)
			grpcServer.GracefulStop()
		}
	}()

	grpcServer.Serve(lis)
}
