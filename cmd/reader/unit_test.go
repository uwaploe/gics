package main

import (
	"testing"

	"github.com/BurntSushi/toml"
	"github.com/imdario/mergo"
)

var EXTRA = `
[[rfgps.nodes]]
id = 5
name = "h1"
[[rfgps.nodes]]
id = 3
name = "h2"
`

func TestConfig(t *testing.T) {
	var cfg, cfg2 sysConfig
	err := toml.Unmarshal([]byte(DEFAULT), &cfg)
	if err != nil {
		t.Fatalf("Cannot parse config file: %v", err)
	}

	if cfg.Rfgps.Device != "/dev/ttyS2" {
		t.Errorf("Bad RF-GPS device: %q", cfg.Rfgps.Device)
	}

	if len(cfg.Rfgps.Nodes) != 0 {
		t.Errorf("Wrong node count: %d", len(cfg.Rfgps.Nodes))
	}

	err = toml.Unmarshal([]byte(EXTRA), &cfg2)
	if err != nil {
		t.Fatalf("Cannot parse config file: %v", err)
	}

	if len(cfg2.Rfgps.Nodes) != 2 {
		t.Errorf("Wrong node count in extra config: %d", len(cfg2.Rfgps.Nodes))
	}

	mergo.Merge(&cfg2, cfg)

	if cfg2.Rfgps.Device != "/dev/ttyS2" {
		t.Errorf("Bad RF-GPS device (extra): %q", cfg2.Rfgps.Device)
	}
}
