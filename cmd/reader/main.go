// Package main implements a reader for the Camp (origin) GPS and the
// remote RF-GPS units. Data records are published via Redis.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/mfkenney/go-gps"
	"bitbucket.org/mfkenney/go-rfgps"
	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/gics/pkg/synctick"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/imdario/mergo"
	"github.com/tarm/serial"
)

type redisConfig struct {
	Address string `toml:"address"`
	Channel string `toml:"channel"`
}

type portConfig struct {
	Device string `toml:"device"`
	Baud   int    `toml:"baud"`
}

type nodeConfig struct {
	Id   uint   `toml:"id"`
	Name string `toml:"name"`
}

type rfgpsConfig struct {
	portConfig
	Vmin  float32      `toml:"vmin"`
	Nodes []nodeConfig `toml:"nodes"`
}

type gpsConfig struct {
	portConfig
	Name string `toml:"name"`
}

type sysConfig struct {
	Redis redisConfig `toml:"redis"`
	Gps   gpsConfig   `toml:"gps"`
	Rfgps rfgpsConfig `toml:"rfgps"`
}

// Alert messages published on Redis
type Alert struct {
	Timestamp int64  `json:"timestamp"`
	Text      string `json:"text"`
}

var Version = "dev"
var BuildDate = "unknown"

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	cfgfile = flag.String("cfgfile", "",
		"Path to the configuration file")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	interval = flag.Duration("interval", time.Second*10,
		"RF-GPS sample interval")
)

const DEFAULT = `
[redis]
address = "localhost:6379"
channel = "data.refs"
[gps]
device = "/dev/ttyS1"
baud = 4800
name = "origin"
[rfgps]
device = "/dev/ttyS2"
baud = 19200
vmin = 12.0
`

type badState struct {
	reason  string
	context interface{}
}

func (bs *badState) Error() string {
	return bs.reason
}

func processRfgpsRecord(rec *rfgps.Record) (*api.Point, error) {
	var err error
	if rec.Status != "A" {
		return nil, &badState{
			reason:  "Bad fix",
			context: *rec,
		}
	}
	if rec.Vbatt < 12.0 {
		err = &badState{
			reason:  "Low voltage",
			context: *rec,
		}
	}

	return &api.Point{
		Latitude:  int32(rec.Lat * api.GEOSCALE),
		Longitude: int32(rec.Lon * api.GEOSCALE),
		Tsec:      rec.T.Unix(),
	}, err
}

func processGpsRecord(rec *gps.Record) (*api.Point, error) {
	var err error
	if rec.Status != "A" {
		return nil, &badState{
			reason:  "Bad fix",
			context: *rec,
		}
	}

	return &api.Point{
		Latitude:  int32(rec.Lat * api.GEOSCALE),
		Longitude: int32(rec.Lon * api.GEOSCALE),
		Tsec:      rec.T.Unix(),
	}, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "Read ICEX GPS receivers\n\n")
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var (
		defcfg, cfg sysConfig
		contents    []byte
		err         error
	)

	err = toml.Unmarshal([]byte(DEFAULT), &defcfg)
	if err != nil {
		log.Fatalf("Cannot parse default configuration: %v", err)
	}

	if *cfgfile != "" {
		contents, err = ioutil.ReadFile(*cfgfile)
		if err != nil {
			log.Fatal(err)
		}
		err = toml.Unmarshal(contents, &cfg)
		if err != nil {
			log.Fatalf("Cannot parse config file: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)

	// Open RF-GPS serial port
	c := &serial.Config{
		Name:        cfg.Rfgps.Device,
		Baud:        cfg.Rfgps.Baud,
		ReadTimeout: time.Second * 4}
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}
	rfgps_dev := rfgps.NewRfgps(s)
	rfgps_dev.SetDebug(*debug)

	// Open GPS serial port
	c = &serial.Config{
		Name:        cfg.Gps.Device,
		Baud:        cfg.Gps.Baud,
		ReadTimeout: time.Second * 3}
	s, err = serial.OpenPort(c)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}
	gps_dev := gps.NewGps(s)
	gps_dev.SetDebug(*debug)

	// Connect to Redis server
	conn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis server: %v", err)
	}

	// Map the RF-GPS node IDs to names
	node_names := make(map[uint]string)
	for _, node := range cfg.Rfgps.Nodes {
		if node.Name == "" {
			node_names[node.Id] = fmt.Sprintf("rfgps-%d", node.Id)
		} else {
			node_names[node.Id] = node.Name
		}
	}
	log.Printf("Using %d RF-GPS nodes\n", len(cfg.Rfgps.Nodes))

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	tm := synctick.NewSyncTicker(*interval)
	pset := api.PointSet{}
	ma := jsonpb.Marshaler{EmitDefaults: true}

loop:
	for {
		select {
		case <-tm.C:
			pset.Points = make(map[string]*api.Point)
			for addr, name := range node_names {
				rec, err := rfgps_dev.Poll(addr)
				if err != nil {
					log.Printf("Error: %v", err)
					continue loop
				} else {
					if msg, err := json.Marshal(rec); err == nil {
						conn.Do("PUBLISH", "data.rfgps", msg)
					}
					if rec.Vbatt < cfg.Rfgps.Vmin {
						a := &Alert{
							Timestamp: time.Now().UTC().Unix(),
							Text: fmt.Sprintf("Low battery (%.1fV) on RFGPS-%d",
								rec.Vbatt, rec.Address),
						}
						log.Println(a.Text)
						if msg, err := json.Marshal(a); err == nil {
							conn.Do("PUBLISH", "grid.alert", msg)
						}
					}
					p, err := processRfgpsRecord(rec)
					if p != nil {
						p.Id = name
						pset.Points[p.Id] = p
					}
					if err != nil {
						log.Print(err)
					}
				}
			}
			rec, err := gps_dev.Poll()
			if err != nil {
				log.Printf("Error: %v", err)
				continue loop
			} else {
				if msg, err := json.Marshal(rec); err == nil {
					conn.Do("PUBLISH", "data.gps", msg)
				}
				p, err := processGpsRecord(rec)
				if p != nil {
					p.Id = cfg.Gps.Name
					pset.Points[p.Id] = p
				}
				if err != nil {
					log.Print(err)
				}
			}

			msg, err := ma.MarshalToString(&pset)
			if err != nil {
				log.Printf("Encoding error: %v", err)
			} else {
				conn.Do("PUBLISH", cfg.Redis.Channel, msg)
			}
		case <-sigs:
			break loop
		}
	}
	tm.Stop()

}
