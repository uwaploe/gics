// Archive2csv is used to convert one or more JSON archive files to CSV. At the
// moment, only GPS and GRID data files are supported.
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	gps "bitbucket.org/mfkenney/go-gps"
	rfgps "bitbucket.org/mfkenney/go-rfgps"
	"bitbucket.org/uwaploe/gics/api"
	"github.com/golang/protobuf/jsonpb"
	"github.com/pkg/errors"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: archive2csv [options] infile [infile ...]

Convert one or more newline-delimited JSON archive files to CSV. The output
files are given the same base-name as the input files with the file extension
replaced with ".csv".
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	outdir = flag.String("outdir", ".", "Output directory")
)

func exportRfgps(scanner *bufio.Scanner, w io.Writer) error {
	var rec rfgps.Record
	line := int(1)

	if scanner.Text() != "" {
		err := json.Unmarshal(scanner.Bytes(), &rec)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		rec.ToCsv(w, true)
		line++
	}

	for scanner.Scan() {
		err := json.Unmarshal(scanner.Bytes(), &rec)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		rec.ToCsv(w, false)
		line++
	}

	return scanner.Err()
}

func exportGps(scanner *bufio.Scanner, w io.Writer) error {
	var rec gps.Record
	line := int(1)

	if scanner.Text() != "" {
		err := json.Unmarshal(scanner.Bytes(), &rec)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		rec.ToCsv(w, true)
		line++
	}

	for scanner.Scan() {
		err := json.Unmarshal(scanner.Bytes(), &rec)
		if err != nil {
			log.Println(errors.Wrapf(err, "line %d", line))
		}
		rec.ToCsv(w, false)
		line++
	}

	return scanner.Err()
}

func exportRefs(scanner *bufio.Scanner, w io.Writer) error {
	var ps api.PointSet
	line := int(1)

	if scanner.Text() != "" {
		err := jsonpb.UnmarshalString(scanner.Text(), &ps)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		ps.ToCsv(w, true)
		line++
	}

	for scanner.Scan() {
		err := jsonpb.UnmarshalString(scanner.Text(), &ps)
		if err != nil {
			log.Println(errors.Wrapf(err, "line %d", line))
		}
		ps.ToCsv(w, false)
		line++
	}

	return scanner.Err()
}

func exportGrid(scanner *bufio.Scanner, w io.Writer) error {
	var grid api.Grid
	line := int(1)
	if scanner.Text() != "" {
		err := jsonpb.UnmarshalString(scanner.Text(), &grid)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		grid.ToCsv(w, true)
		line++
	}

	for scanner.Scan() {
		err := jsonpb.UnmarshalString(scanner.Text(), &grid)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
		grid.ToCsv(w, false)
		line++
	}

	return scanner.Err()
}

func convertFile(r io.Reader, w io.Writer) error {
	var record map[string]interface{}
	scanner := bufio.NewScanner(r)

	// Read the first record to determine the type of data in the file.
	if scanner.Scan() {
		err := json.Unmarshal(scanner.Bytes(), &record)
		if err != nil {
			return err
		}
		_, valid := record["points"]
		if valid {
			log.Println("Reference point data records")
			return exportRefs(scanner, w)
		}
		_, valid = record["azimuth"]
		if valid {
			log.Println("Grid data records")
			return exportGrid(scanner, w)
		}
		_, valid = record["address"]
		if valid {
			log.Println("RF-GPS data records")
			return exportRfgps(scanner, w)
		}
		_, valid = record["hdop"]
		if valid {
			log.Println("GPS data records")
			return exportGps(scanner, w)
		}

		log.Println("Unsupported record type")
	}

	return scanner.Err()
}

func processFile(filename string) error {
	infile, err := os.Open(filename)
	if err != nil {
		return errors.Wrapf(err, "open %q", filename)
	}
	defer infile.Close()

	name := filepath.Base(filename)
	if i := strings.LastIndex(name, "."); i != -1 {
		name = name[:i] + ".csv"
	} else {
		name = name + ".csv"
	}
	outpath := filepath.Join(*outdir, name)
	outfile, err := os.Create(outpath)
	if err != nil {
		return errors.Wrapf(err, "open %q", outpath)
	}
	defer outfile.Close()

	log.Printf("%q -> %q\n", filename, outpath)
	return convertFile(infile, outfile)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing input files ...")
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *outdir != "." {
		os.MkdirAll(*outdir, 0755)
	}

	for _, arg := range args {
		if err := processFile(arg); err != nil {
			log.Println(err)
		}
	}
}
