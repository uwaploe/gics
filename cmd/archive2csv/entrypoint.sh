#!/bin/bash

if [[ $# == 0 ]]; then
    # Convert all archive files under /data and write to /output
    find /data -name '*.json' -print | xargs archive2csv --outdir /output
else
    exec "$@"
fi
