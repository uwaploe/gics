#!/usr/bin/env bash
#
# Access Redis running in a Docker container
#

: ${CONTAINER=gics_redis_1}

docker exec -it \
       $CONTAINER redis-cli "$@"
