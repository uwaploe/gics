#!/usr/bin/env bash
#
# Continuously monitor the lat/lon of the end-of-run grid location and
# load the position as a waypoint named EOR into a Garmin hand-held GPS
# connected to a serial port.
#

network_active ()
{
    n=$(docker network inspect "$1" | jq '.[0].Containers | length')
    if ((n > 0)); then
        return 0
    else
        return 1
    fi
}

gpsdev="$1"
x="$2"
y="$3"

[[ -z $gpsdev || -z $x || -z $y ]] && {
    echo "Usage: loadeor.sh gpsdev X Y"
    exit 1
}

if network_active gicssim_icexnet; then
    network="gicssim_icexnet"
else
    network="gics_icexnet"
fi

docker run -it --rm --network $network \
       -e REDIS_HOST=redis:6379 \
       -e MAPPER_HOST=mapper:10000 \
       watchxy EOR $x $y | \
    {
        while read header; do
            read data
            echo -e "${header}\n${data}"
            echo -e "${header}\n${data}" |\
                gpsbabel -w -i unicsv,grid=ddd -f - -o garmin -F $gpsdev
        done
    }
