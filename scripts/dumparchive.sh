#!/usr/bin/env bash
#
# Dump the contents of the ICEX data archive to a set of CSV files.
#

: ${IMAGE="archive2csv:latest"}
: ${VOLUME="gics_data-archive"}

outdir="${1:-$PWD}"
docker run --rm \
       -v "${VOLUME}:/data" \
       -v "${outdir}:/output" \
       $IMAGE && \
    find "$outdir" -type f -empty -exec rm -f {} \;
