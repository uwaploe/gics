package api

// Scale factor for geodetic positions
const GEOSCALE float64 = 1e7

// Scale factor for grid positions
const GRIDSCALE float64 = 1e3

// Scale factor for azimuth
const ANGLESCALE float64 = 1e3

// Conversion factor between yards (US) and meters
const YDS_PER_METER float64 = 1. / 0.914401828803658
