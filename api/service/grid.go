package service

import (
	"fmt"
	"io/ioutil"
	"math"
	"sync"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/gics/pkg/ema"
	"github.com/golang/protobuf/jsonpb"
	"github.com/pebbe/go-proj-4/proj"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

// Maintain state for the gRPC server
type GridServer struct {
	mu       *sync.RWMutex
	azimuth  float64
	units    string
	pgrid    *proj.Proj
	pgeo     *proj.Proj
	t_update time.Time
	vfilt    *ema.VecEma
}

type GridServerOption struct {
	f func(*gridOptions)
}

type gridOptions struct {
	infile      string
	units       string
	filterCoeff float64
}

// InitialGridFile specifies the name of a JSON file containing the initial
// grid settings.
func InitialGridFile(filename string) GridServerOption {
	return GridServerOption{func(opts *gridOptions) {
		opts.infile = filename
	}}
}

// GridUnits specifies the units used for the grid coordinates. Any unit supported
// by the PROJ.4 Cartographic Projections Library is allowed. See the URL below
// for details:
//
// http://proj4.org/parameters.html#units
//
func GridUnits(units string) GridServerOption {
	return GridServerOption{func(opts *gridOptions) {
		opts.units = units
	}}
}

// FilterCoefficient specifies the coefficient for the exponential moving average
// filter applied to the velocity estimate for the grid.
func FilterCoefficient(alpha float64) GridServerOption {
	return GridServerOption{func(opts *gridOptions) {
		opts.filterCoeff = alpha
	}}
}

// NewGridServer creates a new GridServer instance and returns any error
// that occurs.
func NewGridServer(options ...GridServerOption) (*GridServer, error) {
	var err error

	opts := gridOptions{
		units:       "us-yd",
		filterCoeff: 0.064,
	}
	for _, opt := range options {
		opt.f(&opts)
	}

	s := new(GridServer)
	s.mu = &sync.RWMutex{}

	s.pgeo, err = proj.NewProj("+proj=latlong")
	if err != nil {
		return nil, errors.Wrap(err, "define projection")
	}

	s.units = opts.units
	s.vfilt = ema.NewFilter(opts.filterCoeff, 2)

	if opts.infile != "" {
		file, err := ioutil.ReadFile(opts.infile)
		if err != nil {
			return nil, errors.Wrap(err, "read grid file")
		}

		var grid api.Grid
		if err := jsonpb.UnmarshalString(string(file), &grid); err != nil {
			return nil, errors.Wrap(err, "decode grid file")
		}

		err = s.UpdateState(&grid)
		if err != nil {
			return nil, err
		}
	}

	return s, nil
}

// UpdateState updates the grid origin and azimuth
func (s *GridServer) UpdateState(grid *api.Grid) error {
	var err error
	pdef := fmt.Sprintf(
		"+proj=tmerc +ellps=WGS84 +units=%s +lat_0=%.6f +lon_0=%.6f",
		s.units,
		float64(grid.Origin.Latitude)/api.GEOSCALE,
		float64(grid.Origin.Longitude)/api.GEOSCALE)
	pgrid, err := proj.NewProj(pdef)

	if err != nil {
		return errors.Wrap(err, "cannot define grid projection")
	}

	// Calculate the East-North drift distances since the last
	// grid update.
	var (
		t           time.Time
		east, north float64
	)

	gp, err := s.ToGrid(context.TODO(), grid.Origin)
	if err == nil {
		t = time.Unix(int64(grid.Tsec), 0).UTC()
		dx := float64(gp.X) / api.GRIDSCALE
		dy := float64(gp.Y) / api.GRIDSCALE
		cos := math.Cos(s.azimuth)
		sin := math.Sin(s.azimuth)
		east = dx*cos - dy*sin
		north = dy*cos + dx*sin
	}

	s.mu.Lock()
	if !s.t_update.IsZero() && !t.IsZero() {
		dt := float64(t.Sub(s.t_update) / time.Second)
		s.vfilt.Update([]float64{east / dt, north / dt})
	}
	s.t_update = time.Unix(int64(grid.Tsec), 0).UTC()
	s.pgrid = pgrid
	s.azimuth = proj.DegToRad(360. - float64(grid.Azimuth)/api.ANGLESCALE)
	s.t_update = t
	s.mu.Unlock()

	return nil
}

// ToGrid converts a geodetic position (latitude and longitude) to a grid
// position (X, Y)
func (s *GridServer) ToGrid(ctx context.Context,
	p *api.Point) (*api.GridPoint, error) {

	if s.pgrid == nil {
		return nil, errors.New("Grid not defined")
	}

	s.mu.RLock()
	east, north, err := proj.Transform2(s.pgeo, s.pgrid,
		proj.DegToRad(float64(p.Longitude)/api.GEOSCALE),
		proj.DegToRad(float64(p.Latitude)/api.GEOSCALE))
	cos := math.Cos(s.azimuth)
	sin := math.Sin(s.azimuth)
	s.mu.RUnlock()

	if err != nil {
		return nil, err
	}

	// Correct for grid rotation
	x := east*cos + north*sin
	y := north*cos - east*sin

	return &api.GridPoint{
		X:    int32(x * api.GRIDSCALE),
		Y:    int32(y * api.GRIDSCALE),
		Id:   p.Id,
		Tsec: p.Tsec,
	}, nil
}

// ToGeo converts a grid position to a geodetic position.
func (s *GridServer) ToGeo(ctx context.Context,
	p *api.GridPoint) (*api.Point, error) {

	if s.pgrid == nil {
		return nil, errors.New("Grid not defined")
	}

	x := float64(p.X) / api.GRIDSCALE
	y := float64(p.Y) / api.GRIDSCALE

	s.mu.RLock()
	// Correct for grid rotation
	cos := math.Cos(s.azimuth)
	sin := math.Sin(s.azimuth)
	east := x*cos - y*sin
	north := y*cos + x*sin

	lon, lat, err := proj.Transform2(s.pgrid, s.pgeo, east, north)
	s.mu.RUnlock()

	if err != nil {
		return nil, err
	}
	return &api.Point{
		Latitude:  int32(proj.RadToDeg(lat) * api.GEOSCALE),
		Longitude: int32(proj.RadToDeg(lon) * api.GEOSCALE),
		Id:        p.Id,
		Tsec:      p.Tsec,
	}, nil
}

// GetDrift returns a course and speed estimate for the grid
func (s *GridServer) GetDrift(ctx context.Context, _ *api.Empty) (*api.Drift, error) {
	d := api.Drift{}

	s.mu.RLock()
	if !s.t_update.IsZero() {
		d.Tsec = s.t_update.Unix()
	}
	v := s.vfilt.State()
	s.mu.RUnlock()

	if v != nil {
		d.Course = int32(math.Atan2(v[0], v[1]) * api.ANGLESCALE)
		d.Speed = int32(math.Sqrt(v[0]*v[0]+v[1]*v[1]) * api.GRIDSCALE)
	}

	return &d, nil
}
