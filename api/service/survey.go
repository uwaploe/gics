package service

import (
	"fmt"
	"math"
	"sync"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/gics/pkg/ema"
	"bitbucket.org/uwaploe/gics/pkg/survey"
	"github.com/kellydunn/golang-geo"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

// Filter implements a moving-average filter
type Filter interface {
	// Update adds a new point and returns the new state
	Update([]float64) ([]float64, error)
	// State returns the current state
	State() []float64
}

// SurveyServer maintains the state for the gPRC server
type SurveyServer struct {
	ids       []string
	idmap     map[string]int
	origin    *geo.Point
	mu        *sync.RWMutex
	ranges    Filter
	azimuths  Filter
	units     string
	cache     map[string]*api.Point
	publisher func(*api.RangeMatrix)
}

type SurveyServerOption struct {
	f func(*surveyOptions)
}

type surveyOptions struct {
	ids       []string
	alpha     float64
	units     string
	publisher func(*api.RangeMatrix)
}

// ReferencePoints specifies the names of the reference points for the
// survey. The default value is []string{"origin", "h1", "h2", "h3", "h4"}
func ReferencePoints(ids []string) SurveyServerOption {
	return SurveyServerOption{func(opts *surveyOptions) {
		opts.ids = make([]string, len(ids))
		copy(opts.ids, ids)
	}}
}

// SmoothingFactor specifies the coefficient of the exponential moving
// average filters applied to the calculated ranges and bearings. The
// default value is 0.1
func SmoothingFactor(alpha float64) SurveyServerOption {
	return SurveyServerOption{func(opts *surveyOptions) {
		opts.alpha = alpha
	}}
}

// SurveyUnits specifies the units used for the grid coordinates. Any unit supported
// by the PROJ.4 Cartographic Projections Library is allowed. See the URL below
// for details:
//
// http://proj4.org/parameters.html#units
//
// The default value is "us-yd"
func SurveyUnits(units string) SurveyServerOption {
	return SurveyServerOption{func(opts *surveyOptions) {
		opts.units = units
	}}
}

// Publisher specifies a function which will receive a copy of the current
// RangeMatrix everytime UpdatePoints is called.
func Publisher(pub func(rm *api.RangeMatrix)) SurveyServerOption {
	return SurveyServerOption{func(opts *surveyOptions) {
		opts.publisher = pub
	}}
}

// Get_ranges returns a range matrix for a set of points.
func get_ranges(ps *api.PointSet, ids []string) ([]float64, error) {
	points := make([]*geo.Point, 0)
	for _, id := range ids {
		p, found := ps.Points[id]
		if !found {
			return nil, errors.Errorf("Point not found: %q", id)
		}
		gp := geo.NewPoint(
			float64(p.Latitude)/api.GEOSCALE,
			float64(p.Longitude)/api.GEOSCALE)
		points = append(points, gp)
	}

	return survey.Ranges(points), nil
}

// Get_bearings returns a bearing matrix for a set of points.
func get_bearings(ps *api.PointSet, ids []string) ([]float64, error) {
	points := make([]*geo.Point, 0)
	for _, id := range ids {
		p, found := ps.Points[id]
		if !found {
			return nil, errors.Errorf("Point not found: %q", id)
		}
		points = append(points, geo.NewPoint(
			float64(p.Latitude)/api.GEOSCALE,
			float64(p.Longitude)/api.GEOSCALE))
	}

	return survey.Bearings(points), nil
}

// NewSurveyServer returns a new SurveyServer instance with a given
// set of options.
func NewSurveyServer(options ...SurveyServerOption) *SurveyServer {
	opts := surveyOptions{
		units: "us-yd",
		alpha: 0.1,
		ids:   []string{"origin", "h1", "h2", "h3", "h4"},
	}
	for _, opt := range options {
		opt.f(&opts)
	}

	s := new(SurveyServer)
	s.mu = &sync.RWMutex{}
	n := len(opts.ids)
	s.units = opts.units
	s.ranges = ema.NewFilter(opts.alpha, n*n)
	s.azimuths = ema.NewFilter(opts.alpha, n*n)
	s.ids = make([]string, n)
	copy(s.ids, opts.ids)
	s.idmap = make(map[string]int)
	for i, id := range s.ids {
		s.idmap[id] = i
	}
	s.cache = make(map[string]*api.Point)
	s.publisher = opts.publisher
	return s
}

// UpdatePoints updates the reference point locations and returns a new
// range matrix.
func (s *SurveyServer) UpdatePoints(ctx context.Context,
	ps *api.PointSet) (*api.RangeMatrix, error) {
	rm := api.RangeMatrix{}
	rs, err := get_ranges(ps, s.ids)
	if err != nil {
		return nil, errors.Wrap(err, "cannot create range matrix")
	} else {
		azs, err := get_bearings(ps, s.ids)
		if err != nil {
			return nil, errors.Wrap(err, "cannot create bearing matrix")
		} else {
			op, found := ps.Points["origin"]
			if !found {
				return nil, errors.New("no origin point supplied")
			} else {
				rm.N = uint32(len(s.ids))
				rm.Ids = make([]string, rm.N)
				rm.Ranges = make([]uint32, rm.N*rm.N)
				rm.Azimuths = make([]int32, rm.N*rm.N)
				copy(rm.Ids, s.ids)

				s.mu.Lock()
				s.ranges.Update(rs)
				s.azimuths.Update(azs)
				s.origin = geo.NewPoint(
					float64(op.Latitude)/api.GEOSCALE,
					float64(op.Longitude)/api.GEOSCALE)
				for k, v := range ps.Points {
					s.cache[k] = v
				}
				s.mu.Unlock()
				s.mu.RLock()
				for i, x := range s.ranges.State() {
					rm.Ranges[i] = uint32(x * api.GRIDSCALE)
				}
				for i, x := range s.azimuths.State() {
					rm.Azimuths[i] = int32(x * api.GRIDSCALE)
				}
				rm.Tsec = time.Now().UTC().Unix()
				s.mu.RUnlock()
			}
		}
	}

	if s.publisher != nil {
		s.publisher(&rm)
	}

	return &rm, nil
}

// GetRanges returns the latest range matrix
func (s *SurveyServer) GetRanges(ctx context.Context,
	m *api.Empty) (*api.RangeMatrix, error) {
	rm := api.RangeMatrix{}

	rm.N = uint32(len(s.ids))
	rm.Ids = make([]string, rm.N)
	rm.Ranges = make([]uint32, rm.N*rm.N)
	rm.Azimuths = make([]int32, rm.N*rm.N)
	copy(rm.Ids, s.ids)

	s.mu.RLock()
	if v := s.ranges.State(); v != nil {
		for i, x := range v {
			rm.Ranges[i] = uint32(x * api.GRIDSCALE)
		}
	}

	if v := s.azimuths.State(); v != nil {
		for i, x := range v {
			rm.Azimuths[i] = int32(x * api.GRIDSCALE)
		}
	}

	rm.Tsec = time.Now().UTC().Unix()
	s.mu.RUnlock()

	return &rm, nil
}

// LookupPoint returns the latest location of a reference point
func (s *SurveyServer) LookupPoint(ctx context.Context,
	req *api.PointReq) (*api.Point, error) {
	if p, ok := s.cache[req.Id]; ok {
		return p, nil
	}
	return nil, errors.New("point not found")
}

// AllPoints returns the latest location of all reference points
func (s *SurveyServer) AllPoints(ctx context.Context,
	req *api.Empty) (*api.PointSet, error) {
	ps := api.PointSet{}
	ps.Points = make(map[string]*api.Point)
	s.mu.RLock()
	for k, v := range s.cache {
		ps.Points[k] = v
	}
	s.mu.RUnlock()
	return &ps, nil
}

// SetQuad uses the traditional method of initializing the grid using
// the locations of the four wired hydrophones.
func (s *SurveyServer) SetQuad(ctx context.Context,
	m *api.Empty) (*api.Survey, error) {
	if s.ranges.State() == nil {
		return nil, errors.New("no reference points available")
	}
	if s.origin == nil {
		return nil, errors.New("grid origin not defined")
	}

	// Number of reference points (+ origin)
	n := len(s.ids)
	if n < 5 {
		return nil, errors.New("insufficient reference points")
	}

	// Find the reference points corresponding to each hydrophone. They
	// must be named "hN", where N = (1, 4)
	var found bool
	elems := make([]int, 4)
	for i := 0; i < 4; i++ {
		name := fmt.Sprintf("h%d", i+1)
		elems[i], found = s.idmap[name]
		if !found {
			return nil, errors.Errorf("no ref point for hydrophone %d", i+1)
		}
	}

	// Copy range and bearing matricies
	rs := make([]float64, n*n)
	azs := make([]float64, n*n)
	s.mu.RLock()
	copy(rs, s.ranges.State())
	copy(azs, s.azimuths.State())
	s.mu.RUnlock()

	// Convert ranges from meters to yards
	if s.units == "us-yd" {
		for i, x := range rs {
			rs[i] = x * api.YDS_PER_METER
		}
	}

	// Place the reference points on a quadrilateral with the origin at
	// hydrophone 4 and the Y-axis parallel to the line from hydrophone 2
	// to hydrophone 1.
	refs := survey.Xyfit(rs, n, elems)

	// Locate the origin reference point so we can shift the grid origin
	org := survey.Trilateration(refs[1:4], rs[2:5])

	// Y-axis bearing
	yaz := azs[elems[1]*n+elems[0]]
	cos := math.Cos(yaz * math.Pi / 180.)
	sin := math.Sin(yaz * math.Pi / 180.)

	// Shift and rotate and convert to external format ...
	gps := make(map[string]*api.GridPoint)
	for i, c := range refs {
		x, y := c.X-org.X, c.Y-org.Y
		c.X = x*cos + y*sin
		c.Y = y*cos - x*sin
		name := s.ids[elems[i]]
		gps[name] = &api.GridPoint{
			X:  int32(c.X * api.GRIDSCALE),
			Y:  int32(c.Y * api.GRIDSCALE),
			Id: name,
		}
	}

	return &api.Survey{
		Tsec:   time.Now().Unix(),
		Points: gps,
	}, nil
}

// SetGrid uses a simpler method to initialize the grid using the (smoothed)
// range and bearing to one or more reference points.
func (s *SurveyServer) SetGrid(ctx context.Context,
	m *api.Empty) (*api.Survey, error) {
	if s.ranges.State() == nil {
		return nil, errors.New("no reference points available")
	}
	if s.origin == nil {
		return nil, errors.New("grid origin not defined")
	}

	// Number of reference points (+ origin)
	n := len(s.ids)
	// Extract ranges and bearings from the origin to each
	// reference point (row 0 of each matrix).
	s.mu.RLock()
	rs := s.ranges.State()[1:n]
	azs := s.azimuths.State()[1:n]
	refs, err := survey.GridSurvey(rs, azs, s.origin)
	s.mu.RUnlock()

	if err != nil {
		return nil, errors.Wrap(err, "survey failed")
	}

	// Build the response message
	gps := make(map[string]*api.GridPoint)
	for i, c := range refs {
		name := s.ids[i+1]
		gps[name] = &api.GridPoint{
			X:  int32(c.X * api.GRIDSCALE),
			Y:  int32(c.Y * api.GRIDSCALE),
			Id: name,
		}
	}

	return &api.Survey{
		Tsec:   time.Now().Unix(),
		Points: gps,
	}, nil
}
