package api

import (
	fmt "fmt"
	"io"
	"time"
)

func (ps *PointSet) ToCsv(w io.Writer, writeHeader bool) error {
	var err error
	if writeHeader {
		fmt.Fprint(w, "node,time,latitude,longitude\n")
	}
	for _, p := range ps.Points {
		t := time.Unix(p.Tsec, 0).UTC()
		_, err = fmt.Fprintf(w, "%s,%s,%.6f,%.6f\n",
			p.Id,
			t.Format(time.RFC3339),
			float64(p.Latitude)/GEOSCALE,
			float64(p.Longitude)/GEOSCALE)
	}
	return err
}

func (g *Grid) ToCsv(w io.Writer, writeHeader bool) error {
	t := time.Unix(g.Tsec, 0).UTC()
	if writeHeader {
		fmt.Fprint(w, "time,latitude,longitude,azimuth\n")
	}
	_, err := fmt.Fprintf(w, "%s,%.6f,%.6f,%.2f\n",
		t.Format(time.RFC3339),
		float64(g.Origin.Latitude)/GEOSCALE,
		float64(g.Origin.Longitude)/GEOSCALE,
		float64(g.Azimuth)/ANGLESCALE)
	return err
}
