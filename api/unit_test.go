package api

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/golang/protobuf/jsonpb"
)

func TestJSON(t *testing.T) {
	gp := GridPoint{X: 4200}
	ma := jsonpb.Marshaler{}
	ma.EmitDefaults = true

	msg, err := ma.MarshalToString(&gp)
	if err != nil {
		t.Fatal(err)
	}

	gp2 := make(map[string]interface{})
	if err = json.Unmarshal([]byte(msg), &gp2); err != nil {
		t.Fatal(err)
	}

	if _, ok := gp2["y"]; !ok {
		t.Errorf("No Y coordinate: %+v", gp2)
	}

	t.Log(gp2)
}

func TestPointSetCsv(t *testing.T) {
	table := []struct {
		ps  *PointSet
		out string
	}{
		{
			ps: &PointSet{Points: map[string]*Point{"h1": &Point{
				Id:        "h1",
				Latitude:  471234567,
				Longitude: -1229876543,
				Tsec:      1520039498}}},
			out: "h1,2018-03-03T01:11:38Z,47.123457,-122.987654\n",
		},
	}

	var b bytes.Buffer
	for _, e := range table {
		err := e.ps.ToCsv(&b, false)
		if err != nil {
			t.Fatal(err)
		}
		if b.String() != e.out {
			t.Errorf("Bad value; expected %q, got %q", e.out, b.String())
		}
	}
}

func TestGridCsv(t *testing.T) {
	table := []struct {
		g   *Grid
		out string
	}{
		{
			g: &Grid{Origin: &Point{
				Id:        "origin",
				Latitude:  471234567,
				Longitude: -1229876543,
				Tsec:      1520039498},
				Azimuth: 4200,
				Tsec:    1520039498,
			},
			out: "2018-03-03T01:11:38Z,47.123457,-122.987654,4.20\n",
		},
	}

	var b bytes.Buffer
	for _, e := range table {
		err := e.g.ToCsv(&b, false)
		if err != nil {
			t.Fatal(err)
		}
		if b.String() != e.out {
			t.Errorf("Bad value; expected %q, got %q", e.out, b.String())
		}
	}
}
