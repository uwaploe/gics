package ema

import (
	"testing"
)

func vequal(a, b []float64) bool {
	for i, x := range a {
		if x != b[i] {
			return false
		}
	}
	return true
}

func TestBasic(t *testing.T) {
	filter := NewFilter(1., 3)

	x := []float64{1., 2., 3.}
	result, err := filter.Update(x)

	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	result, err = filter.Update(x)
	if !vequal(result, x) {
		t.Errorf("Bad result: want %v, got %v", x, result)
	}

	x = []float64{1.}
	result, err = filter.Update(x)
	if err == nil {
		t.Errorf("Bad input not detected")
	}
}
