// Package synctick implements a clock synchronous version of
// time.Ticker. The first tick will occur at the next multiple of the
// specified interval.
package synctick

import (
	"time"
)

type SyncTicker struct {
	C      chan time.Time
	cancel chan struct{}
}

func NewSyncTicker(interval time.Duration) *SyncTicker {
	tm := SyncTicker{
		C:      make(chan time.Time, 1),
		cancel: make(chan struct{})}

	go func() {
		tnext := time.Now().Truncate(interval).Add(interval).Sub(time.Now())
		select {
		case t := <-time.After(tnext):
			tm.C <- t
		case <-tm.cancel:
			return
		}

		ticker := time.NewTicker(interval)
	loop:
		for {
			select {
			case t := <-ticker.C:
				tm.C <- t
			case <-tm.cancel:
				ticker.Stop()
				break loop
			}
		}
	}()

	return &tm
}

func (t *SyncTicker) Stop() {
	close(t.cancel)
}
