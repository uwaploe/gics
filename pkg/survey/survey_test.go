package survey

import (
	"github.com/kellydunn/golang-geo"
	"github.com/pebbe/go-proj-4/proj"
	"math"
	"testing"
)

var test_points []*geo.Point

func init() {
	test_points = make([]*geo.Point, 5)
	test_points[0] = geo.NewPoint(73.126597, -149.420463)
	test_points[1] = geo.NewPoint(73.128263, -149.41545)
	test_points[2] = geo.NewPoint(73.128975, -149.431878)
	test_points[3] = geo.NewPoint(73.123057, -149.431287)
	test_points[4] = geo.NewPoint(73.124067, -149.408678)
}

var test_ranges = []float64{
	0, 250., 250., 250., 250.,
	250., 0., 300., 500., 400.,
	250., 300., 0., 400., 500.,
	250., 500., 400., 0., 300.,
	250., 400., 500., 300., 0.,
}

var test_coords = []Coords{
	{X: -400, Y: 0.},
	{X: -400, Y: -300.},
	{X: 0., Y: -300.},
	{X: 0., Y: 0.},
}

var shifted_coords = []Coords{
	{X: -200, Y: 150.},
	{X: -200, Y: -150.},
	{X: 200, Y: -150.},
	{X: 200, Y: 150.},
}

func TestRangeCalc(t *testing.T) {
	n := len(test_points)
	r := sqmatrix(Ranges(test_points))

	if len(r) != n*n {
		t.Errorf("Wrong matrix size: want %d, got %d",
			n*n,
			len(r))
	}

	// Check that diagonal is zero
	for i := 0; i < n; i++ {
		if r.sel(n, i, i) != 0 {
			t.Errorf("Non-zero diagonal element at (%d, %d)", i, i)
		}
	}

	// Check that r[i,j] = r[j,i]
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			if r.sel(n, i, j) != r.sel(n, j, i) {
				t.Errorf("r[%d, %d] != r[%d, %d]", i, j, j, i)
			}
		}
	}

}

func TestFit(t *testing.T) {
	elems := []int{1, 2, 3, 4}
	c := Xyfit(test_ranges, 5, elems)

	for i, val := range test_coords {
		if c[i].X != val.X || c[i].Y != val.Y {
			t.Errorf("Coordinate mismatch: %v != %v", c[i], val)
		}
	}
}

func TestVec(t *testing.T) {
	ix := Coords{X: 1}.tovec()
	iy := Coords{Y: 1}.tovec()
	iz := Coords{Z: 1}.tovec()

	if len(ix) != 3 {
		t.Errorf("Bad vector length: %v", len(ix))
	}

	r := ix.cross(iy)
	if r[0] != 0 || r[1] != 0 || r[2] != 1 {
		t.Errorf("Cross product error: want %v, got %v",
			iz, r)
	}

	r = iy.cross(iz)
	if r[0] != 1 || r[1] != 0 || r[2] != 0 {
		t.Errorf("Cross product error: want %v, got %v",
			ix, r)
	}

	r = iz.cross(ix)
	if r[0] != 0 || r[1] != 1 || r[2] != 0 {
		t.Errorf("Cross product error: want %v, got %v",
			iy, r)
	}

	if ix.dot(iz) != 0 {
		t.Errorf("Dot product error: want 0, got %v", ix.dot(iz))
	}
}

func TestTracking(t *testing.T) {
	ranges := []float64{250., 250., 250.}
	target := Trilateration(test_coords[0:3], ranges)

	if target.X != -200. {
		t.Errorf("Bad X coordinate: want -200, got %v", target.X)
	}

	if target.Y != -150. {
		t.Errorf("Bad Y coordinate: want -150, got %v", target.Y)
	}
}

func TestTrackingShift(t *testing.T) {
	elems := []int{1, 2, 3, 4}
	refs := Xyfit(test_ranges, 5, elems)

	// Translate all of the coordinates so the origin is in the
	// center and the grid is rotated by the true azimuth between
	// refs[1] and refs[0] (0 degrees in this case)
	ranges := []float64{250., 250., 250.}
	target := Trilateration(refs[0:3], ranges)
	cos := math.Cos(0.)
	sin := math.Sin(0.)

	for i, r := range refs {
		x, y := r.X-target.X, r.Y-target.Y
		r.X = x*cos + y*sin
		r.Y = y*cos - x*sin
		if r.X != shifted_coords[i].X || r.Y != shifted_coords[i].Y {
			t.Errorf("Coord mismatch: want %v, got %v",
				shifted_coords[i], r)
		}
	}
}

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func TestSurvey(t *testing.T) {
	scale := float64(0.914401828803658)
	a := proj.RadToDeg(math.Asin(300. / 500.))
	azs := []float64{270. + a,
		270. - a,
		90. + a,
		90. - a}
	// Convert yards to meters
	rs := []float64{250. * scale, 250. * scale, 250. * scale, 250. * scale}
	c, err := GridSurvey(rs, azs, geo.NewPoint(73.126597, -149.420463))
	if err != nil {
		t.Errorf("Survey failed: %v", err)
	}

	if relerr(c[0].X, shifted_coords[0].X) > 0.01 ||
		relerr(c[0].Y, shifted_coords[0].Y) > 0.01 {
		t.Errorf("Wanted %v, got %v", shifted_coords[0], c[0])
	}
}
