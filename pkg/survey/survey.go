// The survey package contains utility functions to support the
// grid survey process for ICEX
package survey

import (
	"fmt"
	"github.com/kellydunn/golang-geo"
	"github.com/pebbe/go-proj-4/proj"
	"github.com/pkg/errors"
	"math"
)

// Square matrix
type sqmatrix []float64

func (m sqmatrix) sel(dim, row, col int) float64 {
	return m[row*dim+col]
}

type vector []float64

func (v vector) dot(v1 vector) float64 {
	r := float64(0)
	for i, x := range v {
		r += (v1[i] * x)
	}
	return r
}

func (v vector) cross(v1 vector) vector {
	r := vector(make([]float64, 3))
	if len(v) != 3 || len(v1) != 3 {
		return r
	}

	r[0] = v[1]*v1[2] - v[2]*v1[1]
	r[1] = v[2]*v1[0] - v[0]*v1[2]
	r[2] = v[0]*v1[1] - v[1]*v1[0]

	return r
}

func (v vector) add(u vector) vector {
	r := vector(make([]float64, 3))
	for i, x := range v {
		r[i] = x + u[i]
	}
	return r
}

func (v vector) sub(u vector) vector {
	r := vector(make([]float64, 3))
	for i, x := range v {
		r[i] = x - u[i]
	}
	return r
}

func (v vector) mult(u vector) vector {
	r := vector(make([]float64, 3))
	for i, x := range v {
		r[i] = x * u[i]
	}
	return r
}

func (v vector) scale(s float64) vector {
	r := vector(make([]float64, 3))
	for i, x := range v {
		r[i] = x * s
	}
	return r
}

type Coords struct {
	X, Y, Z float64
}

func (c Coords) tovec() vector {
	return vector{c.X, c.Y, c.Z}
}

// Ranges generates a matrix of ranges between every pair of points
// in the input slice. The matrix is returned as a 1d slice, where the
// range (in meters) from p[i] to p[j] is stored in array location
// j + i*N where N is the number of points.
func Ranges(p []*geo.Point) []float64 {
	n := len(p)
	r := make([]float64, n*n)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			r[j+i*n] = p[i].GreatCircleDistance(p[j]) * 1e3
			r[i+j*n] = r[j+i*n]
		}
	}
	return r
}

// Bearings generates a matrix of bearings between every pair of points
// in the input slice. The matrix is returned as a 1d slice, where the
// bearing (in degrees True) from p[i] to p[j] is stored in array location
// j + i*N where N is the number of points.
func Bearings(p []*geo.Point) []float64 {
	n := len(p)
	az := make([]float64, n*n)
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			az[j+i*n] = p[i].BearingTo(p[j])
			az[i+j*n] = p[j].BearingTo(p[i])
		}
	}
	return az
}

// Xyfit uses four elements from a range matrix created by Ranges to
// define a quadrilateral with the four elements at the verticies. The
// fourth element is placed at the origin (0, 0) and the Y-axis is
// parallel to the line between the first and second element. Dim is the
// dimension of the range matrix. Elem is the list of element indicies
// within the matrix.
func Xyfit(r []float64, dim int, elem []int) []Coords {
	m := sqmatrix(r)
	c := make([]Coords, len(elem))

	d12 := m.sel(dim, elem[0], elem[1])
	d23 := m.sel(dim, elem[1], elem[2])
	d31 := m.sel(dim, elem[2], elem[0])
	d14 := m.sel(dim, elem[0], elem[3])
	d24 := m.sel(dim, elem[1], elem[3])

	d := (d23*d23 - d31*d31 + d12*d12) / 2. / d12
	e := d12 - d
	a := math.Sqrt(d31*d31 - e*e)
	d9 := (d24*d24 - d14*d14 + d12*d12) / 2. / d12
	e9 := d12 - d9
	a9 := math.Sqrt(d14*d14 - e9*e9)
	e8 := e - e9

	c[0].X = -a9
	c[0].Y = e - e8
	c[1].X = -a9
	c[1].Y = -d - e8
	c[2].X = a - a9
	c[2].Y = -e8
	c[3].X = 0
	c[3].Y = 0

	return c
}

// GridSurvey provides an alternate method for determining the X,Y locations
// of a set of points given their ranges and azimuths from an origin (geodetic
// position).
func GridSurvey(rs, azs []float64, origin *geo.Point) ([]Coords, error) {
	var err error

	// Use Transverse Mercator projection
	pdef := fmt.Sprintf(
		"+proj=tmerc +ellps=WGS84 +units=us-yd +lat_0=%.6f +lon_0=%.6f",
		origin.Lat(),
		origin.Lng())

	pgrid, err := proj.NewProj(pdef)
	if err != nil {
		return nil, errors.Wrap(err, "cannot create projection")
	}

	c := make([]Coords, len(rs))
	for i, r := range rs {
		p := origin.PointAtDistanceAndBearing(r/1.0e3, azs[i])
		c[i].X, c[i].Y, err = proj.Fwd(pgrid, p.Lng(), p.Lat())
		if err != nil {
			return nil, errors.Wrap(err, "projection failed")
		}
	}

	return c, nil
}

// Trilateration provides the coordinates of a target given its ranges
// from a set of reference points.
//
// See: https://en.wikipedia.org/wiki/Trilateration#Preliminary_and_final_computations
//
func Trilateration(refpoints []Coords, r []float64) Coords {
	refs := make([]vector, len(refpoints))
	for i, rp := range refpoints {
		refs[i] = rp.tovec()
	}

	// Transform the coordinate system so the X-axis runs from
	// refs[0] to refs[1]
	e_hat_x := refs[1].sub(refs[0])
	d := math.Sqrt(e_hat_x.dot(e_hat_x))
	// X unit vector
	e_hat_x = e_hat_x.scale(1. / d)

	// X component of the vector from refs[0] to refs[2]
	i := e_hat_x.dot(refs[2].sub(refs[0]))

	// Calculate the Y unit vector
	e_hat_y := refs[2].sub(refs[0]).sub(e_hat_x.scale(i))
	e_hat_y = e_hat_y.scale(1. / math.Sqrt(e_hat_y.dot(e_hat_y)))

	// Y component of the vector from refs[0] to refs[2]
	j := e_hat_y.dot(refs[2].sub(refs[0]))

	// Z unit vector
	//e_hat_z := e_hat_x.cross(e_hat_y)

	// Square the ranges
	r_sq := make([]float64, len(r))
	for i, e := range r {
		r_sq[i] = e * e
	}

	xc := (r_sq[0] - r_sq[1] + d*d) / (2 * d)
	yc := (r_sq[0]-r_sq[2]+i*i+j*j)/(2*j) - (i*xc)/j
	z_sq := r_sq[0] - xc*xc - yc*yc
	zc := math.Copysign(math.Sqrt(math.Abs(z_sq)), z_sq)

	// Transform back to the original coordinate system
	p := refs[0].add(e_hat_x.scale(xc)).add(e_hat_y.scale(yc))

	return Coords{X: p[0], Y: p[1], Z: zc}
}
