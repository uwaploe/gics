# ICEX GICS Software


The GPS Interface Computer System (GICS) processes position data from a
GPS unit located at the grid origin and remote units (RF-GPS or RTR) to
perform the initial grid survey and track the current grid location and
orientation.


## Sensor Sampling

The reader process is responsible for sampling the origin GPS and the
RF-GPS units. The position information is published on a Redis pub-sub
channel as a [Protocol
Buffer](https://developers.google.com/protocol-buffers/docs/overview)
message:

``` protocol-buffer

    message Point {
        // Latitude in degrees * 10**7
        int32 latitude = 1;
        // Longitude in degrees * 10**7
        int32 longitude = 2;
        // Optional ID
        string id = 3;
        // Optional time in seconds since 1/1/1970 UTC
        int64 time = 4;
    }

    message PointSet {
        map<string, Point> points = 1;
    }

```

The generic message format allows for including position data from a
future RTR system. The sampling process ensures that the positions are
valid before publishing.


## Grid Survey

Uses the set of RF-GPS and/or RTR positions to create a range matrix which
can be averaged over time and used to calculate the grid points for each
hydrophone. The survey and the range matrix used to calculate it are both
stored in a [Redis](https://redis.io) data store.

``` protocol-buffer

    message GridPoint {
        // X coordinate in grid units * 10**3
        int32 x = 1;
        // Y coordinate in grid units * 10**3
        int32 y = 2;
        // Optional ID
        string id = 3;
        // Optional time in seconds since 1/1/1970 UTC
        int64 time = 4;
    }

    message Survey {
        // Time in seconds since 1/1/1970 UTC
        int64 tsec = 1;
        // Reference points
        map<string, GridPoint> points = 2;
    }

    message RangeMatrix {
        // Number of points
        uint32 n = 1;
        // point IDs (length == n)
        repeated string ids = 2;
        // Distance in millimeters (length == n*n)
        repeated uint32 ranges = 3;
        // Azimuth in milli-degrees (length == n*n)
        repeated int32 azimuths = 4;
        // Time in seconds since 1/1/1970 UTC
        int64 tsec = 5;
    }

```

The survey process requires the following input parameters:

-   A set of Point IDs that must be present in a PointSet.
-   An
    [EMA](https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average)
    filter coefficent to apply to the range matrix values.

The survey process listens for PointSet messages on a Redis pub-sub
channel. When a message is received, it is used to update the current
range matrix. A [gRPC](https://grpc.io) function is provided to perform
the survey. The caller is responsible for storing the survey data.


## Grid Monitoring

Monitors the set of RF-GPS positions for:

1.  Changes in their grid coordinates relative to last survey
2.  Changes in their grid bearing relative to the GPS bearing which results
    in an update to the grid azimuth.

The coordinate/bearing changes will be detected by comparing the most
recent Range Matrix with the one stored at the time of the last
survey. The difference between the two matricies will be published on the
*data.offsets* Redis pub-sub channel. The grid origin and azimuth values
are published on the *data.grid* channel.

``` protocol-buffer

    message Grid {
        // Time in seconds since 1/1/1970 UTC
        int64 tsec = 1;
        // Grid origin
        Point origin = 2;
        // Y-axis azimuth in millidegrees True
        int32 azimuth = 3;
    }

```

## Mapper

This process subscribes to the *data.grid* channel and provides a
[gRPC](https://grpc.io) server to convert between grid and geodetic
coordinates.

``` protocol-buffer
service Mapper {
    // Convert a geodetic position to a grid position
    rpc ToGrid (Point) returns (GridPoint) {}
    // Convert a grid position to a geodetic position
    rpc ToGeo (GridPoint) returns (Point) {}
}
```

## User Interface

Two web applications will provide the User Interface. One UI for the grid
surveying process and the other for the grid monitoring process. The
source code for those applications are in separate repositories:

- https://bitbucket.org/uwaploe/icex-survey-app
- https://bitbucket.org/uwaploe/icex-gps-app


## Pub/Sub Channels

Redis serves as a Message Broker for the GICS tasks.

| *Channel*    | *Message*   | *Producer* | *Consumers*         | *Encoding* |
|--------------|-------------|------------|---------------------|------------|
| data.refs    | PointSet    | reader     | surveyor,monitor,UI | JSON       |
| data.ranges  | RangeMatrix | surveyor   | monitor,UI          | JSON       |
| data.grid    | Grid        | monitor    | mapper,UI           | JSON       |
| data.offsets | RangeMatrix | monitor    | UI                  | JSON       |

### Data Flow Diagram

In the diagram below, the Message Mux is used to multiplex the messages
from one or more channels over a one-way RS232 serial link. This allows
the transmission of messages to a classified computer.

![Dataflow](dataflow.svg "data-flow diagram")
