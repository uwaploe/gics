# Set-up an isolated Python3 development environment
with import <nixpkgs> {};

(pkgs.python36.buildEnv.override {
  extraLibs = with pkgs.python36Packages; [virtualenv pip click];
}).env
