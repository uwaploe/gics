#!/usr/bin/env python

from __future__ import print_function
import click
import grpc
from gics import api_pb2, api_pb2_grpc
from google.protobuf import json_format


@click.command()
@click.argument('server')
def cli(server):
    channel = grpc.insecure_channel(server)
    stub = api_pb2_grpc.SurveyorStub(channel)
    try:
        s = stub.SetGrid(api_pb2.Empty())
    except grpc.RpcError as e:
        print(e)
    else:
        print(json_format.MessageToJson(s))
    rm = stub.GetRanges(api_pb2.Empty())
    print(json_format.MessageToJson(rm))


if __name__ == '__main__':
    cli()
