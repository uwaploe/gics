#!/usr/bin/env python

from __future__ import print_function
import json
import redis
import click
import grpc
from gics import api_pb2, api_pb2_grpc
from google.protobuf import json_format


@click.command()
@click.option('--combo/--no-combo', default=False,
              help='use combined output format')
@click.option('--grid', metavar='FILE',
              help='JSON file defining the Grid')
@click.argument('pfile')
@click.argument('server')
def cli(combo, grid, pfile, server):
    """
    Read a list of JSON-encoded GridPoints from PFILE and call the ToGeo
    function on SERVER to convert them to Points. The converted values are
    written to standard output as a PointSet
    """
    channel = grpc.insecure_channel(server)
    stub = api_pb2_grpc.MapperStub(channel)
    gdesc = api_pb2.Grid()
    if grid:
        # Server subscribes to grid updates on the data.grid
        # pub-sub channel
        rd = redis.StrictRedis()
        with open(grid, 'rb') as f:
            contents = f.read()
            json_format.Parse(contents, gdesc)
            rd.publish('data.grid', contents)

    points = []
    with open(pfile, 'rb') as f:
        points = json.load(f)

    pset = api_pb2.PointSet()
    for point in points:
        p = api_pb2.GridPoint(**point)
        gp = stub.ToGeo(p)
        for attr in ('latitude', 'longitude', 'id', 'tsec'):
            setattr(pset.points[gp.id], attr, getattr(gp, attr))
    if gdesc.HasField('origin'):
        pset.points['origin'].id = 'origin'
        for attr in ('latitude', 'longitude'):
            setattr(pset.points['origin'], attr, getattr(gdesc.origin, attr))
    out = json_format.MessageToJson(pset)
    if grid and combo:
        refs = json.loads(out)
        # MessageToJson writes int64 values as strings (??!!). The following
        # code is a work-around for timestamps
        for ref in refs:
            if 'tsec' in ref:
                ref['tsec'] = int(ref['tsec'])
        gdict = json.loads(json_format.MessageToJson(gdesc))
        if 'tsec' in gdict:
            gdict['tsec'] = int(gdict['tsec'])
        print(json.dumps({'refs': refs, 'grid': gdict},
                         sort_keys=True,
                         indent=4))
    else:
        print(out)


if __name__ == '__main__':
    cli()
