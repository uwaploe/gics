#!/usr/bin/env python

from __future__ import print_function
import json
import click
import grpc
from gics import api_pb2, api_pb2_grpc


@click.command()
@click.argument('pfile')
@click.argument('server')
def cli(pfile, server):
    channel = grpc.insecure_channel(server)
    stub = api_pb2_grpc.MapperStub(channel)
    points = []
    with open(pfile, 'rb') as f:
        points = json.load(f)
    for point in points:
        p = api_pb2.Point(**point)
        gp = stub.ToGrid(p)
        print(repr(gp))


if __name__ == '__main__':
    cli()
