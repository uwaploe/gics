#
# Makefile for GICS applications. This makefile is based on one created for a demo
# gRPC project:
#
#   https://gitlab.com/pantomath-io/demo-grpc/blob/init-makefile/Makefile
#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

# We need the PROJ.4 C library
PC = /Users/mike/.nix-profile/lib/pkgconfig
CC = /usr/bin/clang
GOBUILD = CC=$(CC) PKG_CONFIG_PATH=$(PC) go build

MAPPER_SERVER := "mapper_svc"
SURVEY_SERVER := "surveyor_svc"
READER := "gps_reader"
MONITOR := "grid_monitor"
ARCHIVER := "gics_archiver"
API_OUT := "api/api.pb.go"
PKG := "bitbucket.org/uwaploe/gics"
MAPPER_PKG_BUILD := "$(PKG)/cmd/mapper"
SURVEY_PKG_BUILD := "$(PKG)/cmd/survey"
READER_PKG_BUILD := "$(PKG)/cmd/reader"
MONITOR_PKG_BUILD := "$(PKG)/cmd/monitor"
ARCHIVER_PKG_BUILD := "$(PKG)/cmd/archiver"
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)
CMDDIRS := $(wildcard cmd/*)

.PHONY: all api build_grid build_survey images

all: build_mapper build_survey build_reader build_monitor build_archiver

images: ## Build all Docker images
	for dir in $(CMDDIRS); do \
	    test -e $$dir/Makefile && $(MAKE) -C $$dir image; \
	done

api/api.pb.go: api/api.proto
	@protoc -I api/ \
		-I${GOPATH}/src \
		--go_out=plugins=grpc:api \
		api/api.proto

api: api/api.pb.go ## Auto-generate grpc go sources

dep: ## Get the dependencies
	@go get -v -d ./...

build_mapper: dep api ## Build grid gRPC server
	@$(GOBUILD) -i -v -o $(MAPPER_SERVER) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(MAPPER_PKG_BUILD) \

build_survey: dep api ## Build survey gRPC server
	@$(GOBUILD) -i -v -o $(SURVEY_SERVER) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(SURVEY_PKG_BUILD)

build_reader: dep api ### Build GPS reader
	@$(GOBUILD) -i -v -o $(READER) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(READER_PKG_BUILD)

build_monitor: dep api ### Build Grid monitor
	@$(GOBUILD) -i -v -o $(MONITOR) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(MONITOR_PKG_BUILD)

build_archiver: dep api ### Build data archiver
	@$(GOBUILD) -i -v -o $(ARCHIVER) \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(ARCHIVER_PKG_BUILD)

clean: ## Remove previous builds
	@rm -f $(GRID_SERVER) $(SURVEY_SERVER) $(READER) $(MONITOR) $(ARCHIVER)

distclean: clean ## Remove auto-generated files
	@rm -f $(API_OUT)


help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
	  sort | \
	  awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'